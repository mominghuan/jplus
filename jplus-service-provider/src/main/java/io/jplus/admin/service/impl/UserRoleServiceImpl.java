package io.jplus.admin.service.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.UserRole;
import io.jplus.admin.service.UserRoleService;

import java.util.List;


@Bean
@RPCBean
public class UserRoleServiceImpl extends JbootServiceBase<UserRole> implements UserRoleService {

    @Override
    public boolean deleteByUserId(String userId) {
        SqlPara sqlPara = Db.getSqlPara("admin-userrole.deleteByUserId");
        sqlPara.addPara(userId);
        return Db.update(sqlPara) >= 1 ? true : false;
    }

    @Override
    public List<UserRole> findListByUserId(String userId) {
        Columns columns = Columns.create();
        columns.eq("user_id", userId);
        return DAO.findListByColumns(columns);
    }

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}