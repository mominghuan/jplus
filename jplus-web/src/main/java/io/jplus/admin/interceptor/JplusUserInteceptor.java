/**
 * Copyright (c) 2017-2018,Retire 吴益峰 (372310383@qq.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jplus.admin.interceptor;

import com.jfinal.aop.Aop;
import com.jfinal.aop.Invocation;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.utils.CookieUtil;
import io.jboot.utils.StrUtil;
import io.jboot.web.fixedinterceptor.FixedInterceptor;
import io.jplus.JplusConsts;
import io.jplus.admin.model.User;
import io.jplus.admin.service.UserService;

/**
 * @author Retire 吴益峰 （372310383@qq.com）
 * @version V1.0
 * @Package io.jplus.admin.interceptor
 */
public class JplusUserInteceptor implements FixedInterceptor {


    public JplusUserInteceptor() {
        Aop.inject(this);
    }

    @RPCInject
    UserService userService;

    @Override
    public void intercept(Invocation inv) {

        String userId = CookieUtil.get(inv.getController(), JplusConsts.JPLUS_USER_ID);
        if (StrUtil.isNotBlank(userId)) {
            User user = userService.findById(userId);
            if (user != null) {
                inv.getController().setAttr(JplusConsts.JPLUS_USER, user);
                inv.getController().setAttr(JplusConsts.JPLUS_USER_ID, user.getId());
            }
        }

        inv.invoke();
    }
}
